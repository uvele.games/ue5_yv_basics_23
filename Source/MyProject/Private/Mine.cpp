// Fill out your copyright notice in the Description page of Project Settings.


#include "Mine.h"

#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AMine::AMine()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	OverlapSphere = CreateDefaultSubobject<USphereComponent>(TEXT("OverlapSphere"));
	OverlapSphere->SetupAttachment(RootComponent);
	OverlapSphere->SetSphereRadius(SphereRadius);
	OverlapSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapSphere->SetCollisionResponseToAllChannels(ECR_Ignore);
	OverlapSphere->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(OverlapSphere);
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

// Called when the game starts or when spawned
void AMine::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("Begin play called"));
	OverlapSphere->OnComponentBeginOverlap.AddDynamic(this, &AMine::OnSphereOverlap);
	UE_LOG(LogTemp, Warning, TEXT("OnComponentBeginOverlap Subscribed"));
}

void AMine::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                            UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
                            const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("OnSphereOverlap Called"));
	AController* PlayerController = OtherActor->GetInstigator()->GetController();
	const float MineDamage = FMath::Max(1, FMath::RandRange(Damage - RandomDamageDelta, Damage + RandomDamageDelta));
	const float ApplyDamage = UGameplayStatics::ApplyDamage(OtherActor, MineDamage, PlayerController, this, DamageType);
	UE_LOG(LogTemp, Warning, TEXT("OnSphereOverlap Called %f"), ApplyDamage);
	Destroy();
}

// Called every frame
void AMine::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
